require('dotenv').config()
const express = require("express")
const app = express()
const cors = require('cors')
const historyModel = require('./Models/poolHistory')
const Web3 = require('web3');
const web3 = new Web3('wss://bsc-ws-node.nariox.org:443');
// socket
// const http = require('http');
// const server = http.createServer(app);
// const io = require("socket.io");



app.use(cors())

async function init(){
  web3.eth.subscribe(
    'logs',
    {
      // fromBlock: '0x0',
      address: "0xd302797a732A184F9608AE5a0Ec7730567AB46b8",
      topics:[
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        "0x90890809c654f11d6e72a28fa60149770a0d11ec6c92319d6ceb2bb0a4ea1a15"
      ]
    },
    (error) => {
      if (error) {
        return console.error(error);
      }
    }
    )
    .on('connected', (subscriptionId) => {
      console.log('subscriptionId: ' + subscriptionId);
    })
    .on('data', async (log) => {
      console.log('log', log);
      console.log('log', log.address);
      let action = 
        log.topics[0] == '0x90890809c654f11d6e72a28fa60149770a0d11ec6c92319d6ceb2bb0a4ea1a15' 
        ? 'Deposit'
        : 'Withdraw'
      const value = await web3.eth
        .getTransaction(log.transactionHash) 
        .then(res => {
          console.log('res :>> ', res);
          if(res.length > 74){
            console.log(">74");
            return web3.utils.hexToNumber('0x'+res.input.substring(74, 138)).toString()
          }else{
            return web3.utils.toBN(res.input.substring(56,74)).toString()
          }         
        }) 
      console.log('value.toString() :>> ', value);
       await historyModel.create({
        action: action,
        address: log.address,
        amount: value,
        hash: log.transactionHash,
      })
    })
    .on('changed', (log) => console.log(log));
    console.log('tx', await web3.eth.getTransaction('0xbbfe8a06c6db345dff78eab4b6f3c1d62e0b64914b47b49b59ebbe0b00285f87'));
    console.log('object :>> ', web3.utils.toBN('0x1058d28100000000000000000000000000000000000000000000004af3dc29dc20f070e7').toString());  
    

}  
  
init()

app.use('/getAllHisoty', async function (req, res){
  let data =  await historyModel.find({})
  res.send({
    data :data
  }) 
})
 
app.use(function(req, res, next) {
  next(createError(404));
});


// Initialized Socket Server 
// const socketServer = (function () {
//   var instance;
//   function init() {
//     console.log('init socket server')
//     instance = io(server, {
//       cors: {
//         origin: "*",
//         methods: ["GET", "POST", "PUT", "DELETE"],
//         credentials: true
//       }
//     })
//     return instance
//   };
//   return {
//     getInstance: () => instance || init()
//   };
// })();


// var instance = socketServer.getInstance();
// instance.on("connection", (client) => {
//   console.log('client :>> ', client);
// })

// setInterval(() => {
//   console.log('emit to client');
//   instance.emit('CONNETED',{name: "giang"})
// },2000)
app.listen( process.env.PORT | 3000, function () {
    console.log("Success connect");
})